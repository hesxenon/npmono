const { program } = require("commander");
const { exec, execSync } = require("child_process");
const Fs = require("fs");
const Path = require("path");
const chalk = require("chalk");
const R = require("ramda");
const Yaml = require("yaml");
const npmonorc = require("./config");
const { setupCompletion } = require("./completion");
const { forRoot } = require("./list-packages");
const { getTopology, stringifyTopology, parseTopology } = require("./topology");

// ##################################################
// ######################## UTILS ##########################
// ##################################################

const noop = () => {};

const startPrefix = chalk`{cyan ... \u23F3}`;
const donePrefix = chalk`{cyan >>> \u2714}`;
const errorPrefix = chalk`{red !!! \u274C}`;
const infoPrefix = chalk`{blueBright --- \u2139}`;
const logWithPrefix = (prefix) => (...args) => console.log(prefix, ...args);
const log = {
  start: logWithPrefix(startPrefix),
  done: logWithPrefix(donePrefix),
  error: logWithPrefix(errorPrefix),
  info: logWithPrefix(infoPrefix),
};

const errorLogPath = !npmonorc.exists
  ? ""
  : Path.join(Path.dirname(npmonorc.path), ".npmono-error.log");
const infoLogPath = !npmonorc.exists
  ? ""
  : Path.join(Path.dirname(npmonorc.path), ".npmono-info.log");
const logToFile = (file) => (...lines) =>
  Fs.appendFileSync(file, lines.join("\n"));
const logErrorFile = !npmonorc.exists ? noop : logToFile(errorLogPath);
const logInfoFile = !npmonorc.exists ? noop : logToFile(infoLogPath);

const execPromise = ({
  command: createCommand,
  after: [tapSuccess = noop, tapError = noop] = [],
}) => (package) =>
  new Promise((resolve, reject) => {
    const command = createCommand(package);
    exec(command, { cwd: package.path }, (error, stdout, stderr) => {
      logInfoFile(`>>> ${package.json.name} >>>`, `# ${command}`, stdout);
      if (error != null) {
        logErrorFile(`>>> ${package.json.name} >>>`, `# ${command}`, stderr);
        return reject(R.tap(tapError, [package, error, stderr.trim()]));
      }
      resolve(R.tap(tapSuccess, [package, stdout.trim()]));
    });
  });

const nameRegex = /((?<scope>@\S+)\/)?(?<name>\S+?)(?:@(?<version>\S+))?$/;

const updatePackageJson = (fn) => (package) => {
  Fs.writeFileSync(
    `${package.path}/package.json`,
    JSON.stringify(fn(package).json, undefined, package.indent)
  );
};

const excludePackageFilter = (exclude) =>
  R.filter(({ json: { name } }) => name !== exclude);
const limitScope = (scope = []) =>
  R.filter(
    scope.length === 0
      ? () => true
      : ({ json: { name } }) => scope.includes(name)
  );
const onlyIfDependencyFilter = (pkg) =>
  R.filter((package) => {
    if (!(package.hasDependency(pkg) || package.hasDevDependency(pkg))) {
      log.info(
        chalk`{dim skipping package ${package.json.name} because it doesn't list ${pkg} as a dependency}`
      );
      return false;
    }
    return true;
  });

const execParallel = (args) => (packages) =>
  Promise.all(packages.map(execPromise(args)));

const execTopology = ({ topology: topologyParam, ...args }) => (packages) => {
  const topology =
    topologyParam != null ? topologyParam : getTopology(packages);
  log.info(chalk`{dim using topology} '${stringifyTopology(topology)}'`);
  const execPart = (part) =>
    Array.isArray(part) ? execParallel(args)(part) : execPromise(args)(part);
  return topology.reduce(
    (promise, part) => promise.then(() => execPart(part)),
    Promise.resolve()
  );
};

const onlyIfNpmono = (fn) =>
  !npmonorc.exists
    ? () => {
        log.error(
          "command must be executed in an npmono repository. Check if the .npmonorc file exists somewhere along the path to root"
        );
      }
    : fn;

const mkdirP = (path) => {
  if (!Fs.existsSync(path)) {
    const dirname = Path.dirname(path);
    mkdirP(dirname);
    Fs.mkdirSync(path);
  }
};

const byName = (name) => (package) => package.json.name === name;

// ##################################################
// ######################## END UTILS ##########################
// ##################################################

const packages = (() => {
  if (npmonorc.exists) {
    return forRoot(Path.dirname(npmonorc.path))
      .then(
        R.tap(() => {
          const isoTime = new Date().toISOString();
          // clear logs at beginning
          Fs.writeFileSync(errorLogPath, isoTime + "\n");
          Fs.writeFileSync(infoLogPath, isoTime + "\n");
        })
      )
      .catch((e) => {
        log.error(`unexpected error during initializaton, aborting`, e);
        return Promise.resolve([]);
      });
  }
  return Promise.resolve([]);
})();

const upgrade = (pkg, toLatest) => {
  const nameMatches = nameRegex.exec(pkg);
  if (nameMatches == null) {
    return log.error(
      chalk`{red ${pkg} does not conform to the npm package name syntax, aborting!}`
    );
  }
  const pkgName = nameMatches.groups;
  const nameWithoutVersion = [pkgName.scope, pkgName.name]
    .filter(R.complement(R.isNil))
    .join("/");
  return (packages) =>
    execParallel({
      command: ({ hasDevDependency, json }) => {
        log.start(
          chalk`{dim upgrading {reset.bold ${pkg}} in {reset.bold ${json.name.padEnd(
            64
          )}} (dev: ${hasDevDependency(pkg)} / latest: ${!!toLatest})}`
        );
        return toLatest
          ? `npm install ${
              hasDevDependency(pkg) ? "--dev" : ""
            } ${nameWithoutVersion}@latest`
          : `npm upgrade ${pkg}`;
      },
      after: [
        ([package]) => {
          log.done(
            chalk`{dim finished upgrading {reset.bold ${pkg}} in {reset.bold ${package.json.name}}}`
          );
        },
      ],
    })(packages).catch(([package]) => {
      log.error(
        chalk`{dim error when upgrading {reset.bold ${pkg}} in {reset.bold ${package.json.name}}}`
      );
    });
};

const createLink = (package) => {
  const node_modules = Path.resolve(
    Path.dirname(npmonorc.path),
    "node_modules"
  );
  const linkPath = Path.resolve(node_modules, package.json.name);
  if (Fs.existsSync(linkPath)) {
    Fs.unlinkSync(linkPath);
  }
  mkdirP(node_modules);
  mkdirP(
    Path.resolve(
      node_modules,
      ...package.json.name.split(Path.sep).slice(0, -1)
    )
  );
  Fs.symlinkSync(package.path, linkPath);
};

const link = (source, target) => {
  const sourcePath = Path.resolve(
    Path.dirname(npmonorc.path),
    "node_modules",
    source.json.name
  );
  const targetPath = Path.resolve(
    target.path,
    "node_modules",
    source.json.name
  );
  if (Fs.existsSync(targetPath)) {
    Fs.unlinkSync(targetPath);
  }
  mkdirP(
    Path.resolve(
      target.path,
      "node_modules",
      ...source.json.name.split(Path.sep).slice(0, -1)
    )
  );
  Fs.symlinkSync(sourcePath, targetPath);
};

program
  .command("init")
  .description("initializes the current directory as a npmono repo")
  .action(() => {
    npmonorc.write();
    if (npmonorc.content.links != null) {
      packages.then((packages) => {
        Object.entries(npmonorc.content.links).forEach(([source, targets]) => {
          const sourcePackage = packages.find(byName(source));
          if (sourcePackage == null) {
            return log.info(
              `detected broken link: ${source} is not a package in this repo`
            );
          }
          createLink(sourcePackage);
          targets.forEach((target) => {
            const targetPackage = packages.find(byName(target));
            if (targetPackage == null) {
              return log.info(
                `detected broken link target: ${target} is not a package in this repo`
              );
            }
            link(sourcePackage, targetPackage);
          });
        });
      });
    }
  });

program
  .command("config")
  .description("show current configuriaton")
  .action(() => {
    if (Object.keys(npmonorc.content).length > 0) {
      console.log(Yaml.stringify(npmonorc.content));
    }
  });

program
  .command("list")
  .description("list available packages in subfolders")
  .action(
    onlyIfNpmono(() => {
      packages.then(
        R.forEach(({ path, json: { name } }) =>
          console.log(chalk`{bold ${name.padEnd(64)}}{dim (${path})}`)
        )
      );
    })
  );

const scope = ["--scope [scope...]", "limit packages to [scope...]"];

program
  .command("upgrade <pkg>")
  .aliases(["update", "u"])
  .description(
    "upgrade <pkg> in all sub-packages (optionally limited by --scope) that have it listed as a dependency"
  )
  .option(
    "--latest",
    "instead of `npm upgrade <pkg>` this will execute `npm install <pkg>@latest`"
  )
  .option(...scope)
  .action(
    onlyIfNpmono((pkg, { scope, latest }) => {
      packages.then(
        R.pipe(
          limitScope(scope),
          R.filter(({ json: { name } }) => name !== pkg),
          onlyIfDependencyFilter(pkg),
          upgrade(pkg, latest)
        )
      );
    })
  );

program
  .command("add <pkg>")
  .aliases(["install", "i"])
  .description("add <pkg> to all sub-packages (optionally limited by --scope)")
  .option("-D --dev", "add as a dev dependency")
  .option("--peer", "add as a peer dependency")
  .option(...scope)
  .action(
    onlyIfNpmono((pkg, { scope, peer, dev }) => {
      const nameMatches = nameRegex.exec(pkg);
      if (nameMatches == null) {
        log.error(
          chalk`{red does not conform to the npm package name syntax, aborting!}`
        );
        return;
      }
      const version =
        nameMatches.groups.version != null
          ? nameMatches.groups.version
          : peer == null
          ? undefined
          : execSync(`npm show ${pkg} version`, { encoding: "utf-8" }).trim();
      packages
        .then(
          R.pipe(
            limitScope(scope),
            R.filter(({ json: { name } }) => name !== pkg),
            execParallel({
              command: (package) => {
                if (peer) {
                  updatePackageJson((package) => {
                    if (!("peerDependencies" in json)) {
                      package.json.peerDependencies = {};
                    }
                    package.json.peerDependencies[pkg] = `^${version}`;
                    return package;
                  })(package);
                }
                log.start(
                  chalk`{dim adding {reset.bold ${pkg}} in {reset.bold ${package.json.name.padEnd(
                    64
                  )}} (dev: ${!!dev} / peer: ${!!peer})}`
                );
                return `npm i ${dev ? "-D" : ""} ${pkg}`;
              },
              after: [
                ([package]) => {
                  log.done(
                    chalk`{dim added {reset.bold ${pkg}} in {reset.bold ${package.json.name}}}`
                  );
                },
              ],
            })
          )
        )
        .catch(([package]) => {
          log.error(
            chalk`{dim error when adding {reset.bold ${pkg}} in {reset.bold ${package.json.name}}}`
          );
        });
    })
  );

program
  .command("remove <pkg>")
  .aliases(["uninstall", "r"])
  .description(
    "remove <pkg> from all sub-packages (optionally limited by --scope) that have it listed as a dependency"
  )
  .option("--peer", "also remove from peerDependencies")
  .action(
    onlyIfNpmono((pkg, { scope, peer }) => {
      packages
        .then(
          R.pipe(
            limitScope(scope),
            onlyIfDependencyFilter(pkg),
            execParallel({
              command: (package) => {
                updatePackageJson((package) => {
                  if ("peerDependencies" in package.json) {
                    delete package.json.peerDependencies[pkg];
                    if (
                      Object.keys(package.json.peerDependencies).length === 0
                    ) {
                      delete package.json.peerDependencies;
                    }
                  }
                  return package;
                })(package);
                log.start(
                  chalk`{dim removing {reset.bold ${pkg}} from {reset.bold ${
                    package.json.name
                  }} (peer: ${!!peer})}`
                );
                return `npm remove ${pkg}`;
              },
              after: [
                ([package]) => {
                  log.done(
                    chalk`{dim removed {reset.bold ${pkg}} in {reset.bold ${package.json.name}}}`
                  );
                },
              ],
            })
          )
        )
        .catch(([package]) => {
          log.error(
            chalk`{dim error when removing {reset.bold ${pkg}} in {reset.bold ${package.json.name}}}`
          );
        });
    })
  );

program
  .command("publish <pkg> <major|minor|patch>")
  .option("--no-follow", "disable following dependent paths")
  .description(
    "publish a new version of <pkg> and optionally install it in all dependents and publish those as well along the way (--no-follow disables publishing of dependents)"
  )
  .action(
    onlyIfNpmono((pkgParam, version, cmdObj) => {
      if (!/(major|minor|patch)/.test(version)) {
        return log.error(
          chalk`{red invalid version '${version}' passed, expected one of ("major"|"minor"|"patch")}`
        );
      }

      const publish = ({ package, version, publishDependents = true }) => {
        if (package.requiredBy.length === 0) {
          return log.info(
            chalk`{dim won't publish {reset.bold ${package.json.name}} as it's not required by any other package}`
          );
        }
        if (package == null) {
          return log.error(chalk`cannot publish unknown package`);
        }
        if (version == null) {
          return log.error(
            chalk`could not publish ${package.json.name}, no version has been passed!`
          );
        }
        return execPromise({
          command: (package) => {
            log.start(
              chalk`{dim publishing {reset.bold ${package.json.name}} with new {reset.underline ${version}} version}`
            );
            const nextVersion = execSync(`npm version ${version}`, {
              cwd: package.path,
              encoding: "utf-8",
            });
            package.lastVersion = package.json.version; // so we're able to reset in case something goes wrong
            package.json.version = nextVersion.trim();
            return `npm publish`;
          },
          after: [
            ([package]) => {
              delete package.lastVersion; // everything went fine, no reset necessary, reset object to expected state
              log.done(
                chalk`{dim published} {bold ${package.json.name}@{dim ${package.json.version}}}`
              );
            },
            ([package, error]) => {
              execSync(`npm version ${package.lastVersion}`, {
                cwd: package.path,
              });
              delete package.lastVersion; // should be reset now, no need to keep this around
              log.error(
                chalk`{dim could not publish {reset.bold ${package.json.name}}}`,
                error
              );
            },
          ],
        })(package).then(
          !publishDependents
            ? Promise.resolve()
            : ([package]) => {
                return upgrade(
                  package.json.name,
                  true
                )(
                  R.pipe(
                    excludePackageFilter(package.json.name),
                    onlyIfDependencyFilter(package.json.name)
                  )(package.requiredBy)
                ).then(() =>
                  Promise.all(
                    package.requiredBy.map((dependent) =>
                      publish({
                        package: dependent,
                        version: "patch",
                        publishDependents,
                      })
                    )
                  )
                );
              }
        );
      };

      packages
        .then(R.pipe(R.find(({ json: { name } }) => name === pkgParam)))
        .then((package) => {
          if (package == null) {
            return Promise.reject(
              `package ${pkgParam} is not a valid package in this context`
            );
          }
          return publish({
            package,
            version,
            publishDependents: !cmdObj.noFollow,
          });
        })
        .catch((error) => log.error(chalk`${error}`));
    })
  );

program
  .command("whoRequires <pkg>")
  .aliases(["whoUses"])
  .description("list the dependency chain of packages that require <pkg>")
  .action(
    onlyIfNpmono((pkgParam) => {
      packages.then((packages) => {
        const pkg = packages.find(({ json: { name } }) => name === pkgParam);
        console.log(
          pkg.requirePaths
            .map((path) =>
              path.map(({ json: { name } }) => name).join(chalk`{dim  -> }`)
            )
            .join("\n")
        );
      });
    })
  );

program
  .command("topology")
  .description("output the topology of packages")
  .action(
    onlyIfNpmono(() => {
      packages.then(R.pipe(getTopology, stringifyTopology, console.log));
    })
  );

program
  .command("exec")
  .option(
    "--scope [packages...]",
    "limit packages to scope. Incompatible with --topology"
  )
  .option(
    "--topology",
    "override the derived topology like '-s package1 package2 -p package3 package4 -s package5. Incompatible with --scope'"
  )
  .option("--")
  .description("run command in all packages, optionally limited by scope...")
  .action(
    onlyIfNpmono(({ scope, topology, args }) => {
      if (scope != null && topology != null) {
        return log.error(
          `{red invalid usage} cannot combine '--topology' with '--scope'`
        );
      }
      const command = args.join(" ");
      packages
        .then(
          R.pipe(
            limitScope(scope),
            execTopology({
              topology: topology == null ? undefined : parseTopology(topology),
              command: (package) => {
                log.start(
                  chalk`{dim executing '{reset ${command}}' in {reset ${package.json.name}}}`
                );
                return command;
              },
              after: [
                ([package]) => {
                  log.done(
                    chalk`{dim executed '{reset ${command}}' in {reset ${package.json.name}}}`
                  );
                },
                ([package, error]) => {
                  log.error(
                    chalk`{red failed to execute} '${command}' {dim in} ${package.json.name}\n`,
                    error
                  );
                },
              ],
            })
          )
        )
        .catch(noop);
    })
  );

program
  .command("path <pkg>")
  .aliases(["directory", "p"])
  .description("emit the path of the given package")
  .action(
    onlyIfNpmono((pkgParam) => {
      packages.then(
        R.pipe(
          R.find(({ json: { name } }) => name === pkgParam),
          R.prop("path"),
          console.log
        )
      );
    })
  );

program
  .command("link [pkg]")
  .aliases(["ln"])
  .description("link current package into other packages")
  .action(
    onlyIfNpmono((maybePkg) => {
      packages.then((packages) => {
        const package = R.find(
          ({ path }) => process.env.PWD.startsWith(path),
          packages
        );
        if (maybePkg == null) {
          if (package == null) {
            return log.error(
              chalk`{red must be inside a package to be able to link}`
            );
          }
          if (npmonorc.content.links == null) {
            npmonorc.content.links = {};
          }
          if (!(package.json.name in npmonorc.content.links)) {
            npmonorc.content.links[package.json.name] = [];
          }
          npmonorc.write();
          createLink(package);
        } else {
          const targetPackage = packages.find(byName(maybePkg));
          if (targetPackage == null) {
            return log.info(`unknown package '${maybePkg}', aborting...`);
          }
          if (
            npmonorc.content.links == null ||
            !Array.isArray(npmonorc.content.links[maybePkg])
          ) {
            return log.error(
              `tried to link a package that is not available in config, did you run 'npmono link' for ${maybePkg}?`
            );
          }
          if (!npmonorc.content.links[maybePkg].includes(package.json.name)) {
            npmonorc.content.links[maybePkg].push(package.json.name);
            npmonorc.write();
          }
          link(targetPackage, package);
        }
      });
    })
  );

const shouldComplete =
  R.intersection(process.argv, ["--completion", "--compgen"]).length > 0;
const installCompletion =
  R.intersection(process.argv, ["--install-completion"]).length > 0;
const uninstallCompletion =
  R.intersection(process.argv, ["--uninstall-completion"]).length > 0;
if (installCompletion) {
  log.info(chalk`installing autocompletion for npmono`);
  packages
    .then((packages) => setupCompletion(program, packages).setupShellInitFile())
    .catch((e) => log.error("error installing autocompletion", e));
} else if (uninstallCompletion) {
  log.info(chalk`uninstalling autocompletion for npmono`);
  packages
    .then((packages) =>
      setupCompletion(program, packages).cleanupShellInitFile()
    )
    .catch((e) => log.error("error uninstalling autocompletion", e));
} else if (shouldComplete) {
  packages.then((packages) => {
    setupCompletion(program, packages).init();
  });
} else {
  program.parse(process.argv);
}
