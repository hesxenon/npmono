const Fs = require("fs");
const Path = require("path");
const omelette = require("omelette");
const R = require("ramda");

const { getPackageName } = require("./list-packages");

const logToFile = (file) => (...lines) =>
  Fs.appendFileSync(file, lines.join("\n") + "\n");
const log = logToFile(Path.join(__dirname, "log"));
const setupCompletion = (program, packages) => {
  const commandsByName = program.commands.reduce((map, cmd) => {
    map[cmd.name()] = cmd;
    return map;
  }, {});
  const command = ({ reply }) => reply(Object.keys(commandsByName));
  const completion = omelette`npmono ${command}`;
  completion.on("complete", (_, { fragment, line, reply }) => {
    const args = line
      .replace(/^.*npmono/, "npmono")
      .trim()
      .split(" ")
      .slice(0, fragment);
    const command = commandsByName[args[1]];
    const lastArg = R.last(args);
    if (command == null) {
      return;
    }
    const packageNames = packages.map(getPackageName);
    const availableOptions = command.options.map(({ long }) => long);
    const usedOptions = R.intersection(availableOptions, args);
    const remainingOptions = R.difference(availableOptions, usedOptions);
    if (R.last(usedOptions) === "--scope") {
      return reply(packageNames);
    }
    if (
      command.name() === "path" &&
      (lastArg === "path" || packageNames.some(R.startsWith(lastArg)))
    ) {
      return reply(packageNames);
    }
    if (command.name() === "publish") {
      if (packageNames.includes(lastArg)) {
        return reply(["major", "minor", "patch"]);
      }
      if (lastArg === "publish") {
        return reply(packageNames);
      }
    }
    if (command.name() === "link" && lastArg === "link") {
      return reply(packageNames);
    }
    reply(remainingOptions);
  });
  return completion;
};

module.exports = {
  setupCompletion,
};
