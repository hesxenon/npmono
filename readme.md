# Purpose
The purpose of this project is to provide rather low level functions wrapped in a cli to manipulate npm conforming packages in the current npmono directory.

# Non-goals
This project does _**NOT**_
* replace existing monorepo tools like lerna, nx or the likes
* aim to provide *hoisting*.

# Combination with existing monorepo tooling
In theory this shouldn't be a problem from npmonos perspective. Of course you're completely on your own when you modify your sub-packages directly, no guarantee that this won't break functionality from your existing tooling

# A note on hoisting
Imho hoisting creates more problems than it solves and the goal of monorepo tooling is not to reduce disk space usage or even reduce build times. Each package should be completely on its own, with no relation to the monorepo other than that it's possibly being used by other packages.

# API, a.k.a. the meat of this tool
see `# npmono --help`

# Getting started
1. excecute `# npmono init` in the directory you want to specify as your root
1. (optional) `# npmono --install-completion` to try and automatically install tab completion
1. `# npmono --help` to list available commands

# Failed commands and other info
`npmono` create log files upon any execution, commands should log their output into `<npmonoroot>/.npmono-(info|error).log` for their stdout and stderr streams respectively.

# I can't install this cli via npm
Yeah. That's because it isn't in the public npm registry, as I don't have an account there (and don't want one).

You can, however, set this up really easily with this simple command chain.
```bash
(RELEASE=v1.0.1; mkdir -p ${XDG_CONFIG_HOME:-${HOME:-.}}/npmono && cd $_ \
	&& curl -o npmono.zip "https://gitlab.com/hesxenon/npmono/-/archive/$RELEASE/npmono-$RELEASE.zip" \
	&& unzip npmono.zip \
	&& cd npmono-$RELEASE \
	&& npm link \
	&& cd .. && rm npmono.zip)
```

This downloads the latest zip in your $XDG_CONFIG_HOME directory (fallback to $HOME fallback to current directory) and creates a folder called `npmono` where it extracts the zip and runs `npm link` for you. Afterwards it removes the zip file again.

The important part here is just the `npm link` in the directory where npmonos `package.json` is, so you could clone this repo and link it yourself if that's more your style.
