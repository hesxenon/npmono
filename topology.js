const { getPackageName } = require("./list-packages")

const consumeUntil = predicate => remaining => (function consume(consumed) {
	const part = remaining[0]
	return part == null || predicate(part)
	? consumed
	: consume(consumed.concat(remaining.shift()))
})([])

/**
 * returns an array that wraps parallel dependencies into another array and otherwise contains the package
 * so (in package name terms) e.g. ["a", ["b", "c], "d"] would mean to execute in "a" first, then in parallel in "b" and "c" and after that in "d"
 */
const getTopology = packages => {
	const levels = Array.from(packages.reduce((levels, package) => {
		const getLevel = (startLevel, package) => Math.max(startLevel, ...package.requiredBy.map(dependent => getLevel(startLevel + 1, dependent)))
		levels.set(package, getLevel(0, package))
		return levels
	}, new Map()).entries())
	const maxLevel = Math.max(...levels.map(([, level]) => level))
	return Array.from({length: maxLevel + 1}, (_, i) => {
		const ofLevel = levels.reduce((ofLevel, [package, level]) => level !== maxLevel - i ? ofLevel : ofLevel.concat(package), [])
		return ofLevel.length === 1 ? ofLevel[0] : ofLevel
	}).filter(part => Array.isArray(part) ? part.length > 0 : part !== "")
}

const stringifyTopology = topology => {
	const copy = topology.slice()
	const serial = (function readConsume(remaining, serial = []) {
		const part = remaining[0]
		return part != null && !Array.isArray(part)
			? readConsume(remaining, serial.concat(remaining.shift()))
			: serial
	})
	const parts = []
	while(copy.length > 0) {
		!Array.isArray(copy[0])
		? parts.push(`-s ${serial(copy).map(getPackageName).join(" ")}`)
		: parts.push(`-p ${copy.shift().map(getPackageName).join(" ")}`)
	}
	return parts.join(" ")
}

const parseTopology = (string, packages) => {
	const args = string.split(" ")
	const isFlag = arg => arg === "-s" || arg === "-p"
	if(!isFlag(args[0])) {
		throw new Error(`invalid topology string '${string}' expecting ("-s"|"-p") as starting flag`)
	}
	const getPackage = packageName => {
		const package = packages.find(package => getPackageName(package) === packageName)
		if(package == null) {
			throw new Error(`could not find '${packageName}' in passed packages`)
		}
		return package
	}
	const consume = consumeUntil(isFlag)
	let part
	let flag
	const parts = []
	while((flag = args.shift(), part = consume(args)).length > 0) {
		const isParallel = flag === "-p"
		isParallel
		? parts.push(getPackage(part))
		: parts.push(...part.map(getPackage))
	}
	return parts
}

module.exports = {
	getTopology,
	stringifyTopology,
	parseTopology,
}
