const Fs = require("fs");
const Path = require("path");
const Yaml = require("yaml");

const rcname = ".npmonorc";
const existing = (function getRc(path) {
  const rcpath = Path.join(path, rcname);
  if (Fs.existsSync(rcpath)) {
    const content = Yaml.parse(Fs.readFileSync(rcpath, { encoding: "utf8" }));
    return {
      path: rcpath,
      content: content != null ? content : {},
    };
  }
  if (Path.dirname(path) === path) {
    return undefined;
  }
  return getRc(Path.dirname(path));
})(process.env.PWD);
const exists = existing != null;
const npmonorc = exists
  ? existing
  : {
      path: Path.join(process.env.PWD, rcname),
      content: {},
    };

const write = () => {
  const { content, path } = npmonorc;
  Fs.writeFileSync(
    path,
    Object.keys(content).length === 0
      ? "# npmono configuration"
      : (() => {
          const doc = new Yaml.Document();
          doc.contents = content;
          doc.commentBefore = " npmono configuration";
          return doc.toString();
        })()
  );
};

module.exports = {
  ...npmonorc,
  exists,
  write,
};
