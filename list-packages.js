const Fs = require("fs")
const Path = require("path")
const R = require("ramda")

const isDir = file => {
	try {
		return Fs.statSync(file).isDirectory()
	} catch(_) {
		return false
	}
}

const flatten = arr => arr.reduce((flattened, maybeArray) => Array.isArray(maybeArray) ? flattened.concat(flatten(maybeArray)) : (() => {
	flattened.push(maybeArray)
	return flattened
})(), [])

const ignores = [".git"]

const getPackageJsonFile = pkgPath => Fs.readFileSync(`${pkgPath}/package.json`, {encoding: "utf-8"})

const getPackageName = R.pipe(R.prop('json'), R.prop('name'))
const dependencies = R.pipe(R.prop('json'), R.prop('dependencies'), R.defaultTo({}))
const devDependencies = R.pipe(R.prop('json'), R.prop('devDependencies'), R.defaultTo({}))
const peerDependencies = R.pipe(R.prop('json'), R.prop('peerDependencies'), R.defaultTo({}))

const listPackages = (dir) => {
	return new Promise((resolve, reject) => {
		const pending = []
		const packages = []
		Fs.readdir(dir, (err, entries) => {
			if(err) return reject(err)
			ignores.push(...(() => {
				if(entries.find(entry => entry === ".gitignore")) {
					const cleanRegex = /^\*{0,2}\/?(?<pattern>[^\/\n]+).*$/
					return Fs.readFileSync(Path.join(dir, ".gitignore"), {encoding: 'utf-8'}).split("\n")
						.filter(line => line !== '')
						.map(pattern => {
							return cleanRegex.exec(pattern).groups.pattern
						})
				}
				return []
			})())
			entries.forEach(entry => {
				if(ignores.includes(entry)) return
				const path = Path.join(dir, entry)
				if(entry === "package.json") {
					const json = getPackageJsonFile(dir)
					const indent = (() => {
						let level = 0
						let indent = '  '
						for(const line of json.split("\n")) {
							if(line.startsWith("{")) {
								level++
								continue
							}
							const whiteSpace = line.match(/^\s+/)
							if(whiteSpace == null) {
								continue
							}
							if(whiteSpace[0].startsWith("\t")) {
								return "\t"
							} else {
								return Array.from({length: whiteSpace[0].length / level}, () => " ").join("")
							}
						}
						return indent
					})()
					packages.push({
						path: dir,
						indent,
						json: JSON.parse(json),
					})
				} else if(isDir(path)) {
					pending.push(listPackages(path))
				}
			})
			Promise.all(pending)
				.then(subdirEntries => {
					resolve(packages.concat(flatten(subdirEntries)))
				})
				.catch(reject)
		})
	})
}

// const stringify = value => JSON.stringify(value, undefined, 2)
const forRoot = root => {
	return listPackages(root)
		.then(R.uniqBy(getPackageName))
		.then(R.filter(({path}) => path !== root))
		.then(packages => {
			const byId = packages.reduce((map, package) => {
				map[package.json.name] = package
				return map
			}, {})
			const limitToPackages = R.filter(R.flip(R.has)(byId))
			const requireMap = packages.reduce((requireMap, package) => {
				const packageDeps = R.uniq(limitToPackages(Object.keys({
					...dependencies(package),
					...devDependencies(package),
					...peerDependencies(package),
				}))).map(R.flip(R.prop)(byId))
				requireMap[package.json.name].requires = packageDeps
				packageDeps.forEach(dependency => requireMap[dependency.json.name].requiredBy.push(package))
				return requireMap
			}, new Proxy({}, {
				get: (target, prop) => {
					if(!(prop in target)) {
						target[prop] = {
							requiredBy: [],
							requires: [],
						}
					}
					return target[prop]
				}
			}))
			// console.log(stringify({...requireMap}))

			const buildPaths = (package, path=[package], paths=[]) => {
				const requiredBy = requireMap[package.json.name].requiredBy
				if(requiredBy.length === 0) {
					paths.push(path)
					return paths
				}
				requiredBy.forEach((dependent) => buildPaths(dependent, [dependent, ...path], paths))
				return paths
			}
			return packages.map(package => Object.assign(package, {
				hasDependency: R.pipe(R.has, R.applyTo(dependencies(package))),
				hasDevDependency: R.pipe(R.has, R.applyTo(devDependencies(package))),
				hasPeerDependency: R.pipe(R.has, R.applyTo(peerDependencies(package))),
				requiredBy: requireMap[package.json.name].requiredBy,
				dependencies: requireMap[package.json.name].dependencies,
				requirePaths: buildPaths(package),
			}))
		})
}

module.exports = {
	forRoot,
	getPackageName,
}
